# Live Tile Boilerplate

This boilerplate provides a starter-kit for developing customised live tiles for CampusM's homescreens using the [`exlib-livetile-tools`](https://npm.campusm.net/-/docs/@ombiel/exlib-livetile-tools) library

Some live tile examples can be found in the aek package [`exlib-livetile-examples`](https://npm.campusm.net/-/docs/@ombiel/exlib-livetile-examples)

## Installation

First make sure you have the latest version of `aek-cli`

```bash
aek install -g aek-cli
```

You can then create a new AEK project using this boilerplate

```bash
aek create -b exlib-boilerplate-livetiles
```

To get going, simply start the project

```bash
cd myprojectname
aek start
```

## Guide

Please refer to the LiveTile tools documentation for guidance in developing tiles

https://npm.campusm.net/-/docs/@ombiel/exlib-livetile-tools

## Deployment

To build and deploy your assets to our servers, use the standard AEK deploy tool

``` bash
aek deploy
```

The deploy tool should output a list of public paths. Copy the relevant path for your bundle which will look something like `https://portal.ombiel.co.uk/assets/aek/myproject/0.0.1/packed/bundle.js`

You can add this script to your homescreen in App Builder. You can apply multiple scripts either to your entire app or to specific profiles

* Visit App Builder (https://appmanager.ombiel.com/app-builder)
* Select Either the "App Config" or "Profile Config" panel in the right column
* Hit the "code" button in the "config" block for the selected panel
* You should see some JSON code that you need to modify manually
* Add a `scripts` property if one does not already exist - this should be an array of urls to extra scripts you wish to load into the homescreen
* Add the url to your bundle(s) into the array
* Note you can follow the same process if you need to add stylesheets (though we recommend bundling css into your js using css modules)

The final JSON should look something like this

```javascript
{
  //optional
  // "stylesheets": [
  //   "https://portal.ombiel.co.uk/assets/aek/myproject/0.0.1/packed/bundle.css"
  // ],
  "scripts": [
    "https://portal.ombiel.co.uk/assets/aek/myproject/0.0.1/packed/bundle.js",
    "https://portal.ombiel.co.uk/assets/aek/myotherproject/0.0.1/packed/bundle.js",
  ],
  // ... other properties
}
```

At this point, you should be able to preview your livetile instantly in the App Builder preview panel.
